import { Component, OnInit } from '@angular/core';
import {ModalListarProductosComponent} from '../modal-listar-productos/modal-listar-productos.component';
import {ProductoModule} from '../../models/producto/producto.module';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import Swal from 'sweetalert2';
import {InventarioServiceService} from '../../service/inventario-service.service';
import {KardexModule} from '../../models/kardex/kardex.module';
import { CompraModule } from 'src/app/models/compra/compra.module';
import { VentaModule } from 'src/app/models/venta/venta.module';

@Component({
  selector: 'app-kardex',
  templateUrl: './kardex.component.html',
  styleUrls: ['./kardex.component.css']
})
export class KardexComponent implements OnInit {

  constructor(private modalService: BsModalService,public inventarioService:InventarioServiceService) { }

  public producto:ProductoModule;
  public modalRef:BsModalRef;
  public anio:number=2019;
  public mes:number=0;
  public kardex:KardexModule;

  ngOnInit() {
    this.producto=new ProductoModule();
    this.kardex=new KardexModule();
  }

  listaDeProductos(){
    this.modalRef = this.modalService.show(ModalListarProductosComponent,  {
      initialState: {
        title: 'Productos',
        kardexComponent:this
        }, class: "modal-xl" 
    });
  }

  buscarkardex(){
    this.inventarioService.listaVentasPeriodo(this.anio,this.mes,this.producto.idProducto)
    .subscribe(res=>{
      this.kardex.listaVentas=res as VentaModule[];
      console.log(res);
    });

    this.inventarioService.listaComprasPeriodo(this.anio,this.mes,this.producto.idProducto)
    .subscribe(res=>{
      this.kardex.listaCompras= res as CompraModule[];
      console.log(res);
    });
/*
    if(this.kardex.listaCompras.length>0 || this.kardex.listaVentas.length>0)
    {
      Swal.fire({
        type: 'success',
        title: 'Exito, se encontraron registros',
        showConfirmButton: true,
        timer: 1500
      })
    }else{
     alert('error');
    }
*/
  }

}
