import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import {ListarComponent} from '../Compra/listar/listar.component';
import {CompraModule} from '../../models/compra/compra.module';
import {CompraServiceService} from '../../service/compra-service.service';


@Component({
  selector: 'app-modal-detalle-compra',
  templateUrl: './modal-detalle-compra.component.html',
  styleUrls: ['./modal-detalle-compra.component.css']
})
export class ModalDetalleCompraComponent implements OnInit {

  constructor(private formBuilder: FormBuilder,public modalRef: BsModalRef,public compraService:CompraServiceService) { }

  private listarComponent:ListarComponent;
  private compra:CompraModule;
  private formCompra:FormGroup;
  private editar:boolean=false;

  ngOnInit() {

    this.formCompra = this.formBuilder.group({

      idCompra: [''],
      fecha: ['',Validators.required],
      estado:{
          idEstado:'1'
        },
      proveedor:{
        ruc:''
      },
      comprobanteDePago:{
        idComprobanteDePago:''
      },
      usuario:{
        idUsuario:''
      }
     })


  }

  guardar(){
    console.log(this.formCompra.value);
    if (this.formCompra.valid) {

          this.compraService.ingresarCompra(this.formCompra.value).subscribe(res=>{
          this.formCompra.reset();
          if(this.editar)
          {
            Swal.fire('Exito', 'Compra Editada Correctamente!', 'success');  
          }else{
            Swal.fire('Exito', 'Compra Registrada Correctamente!', 'success');
          }
          this.modalRef.hide();
          this.listarComponent.listarCompra();
        });  
      
    }else{
      Swal.fire('Error', 'Debes completar el formulario. Verifique por favor.', 'error'); 
    }
  }


}
