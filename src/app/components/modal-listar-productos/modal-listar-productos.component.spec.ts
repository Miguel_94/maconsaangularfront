import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalListarProductosComponent } from './modal-listar-productos.component';

describe('ModalListarProductosComponent', () => {
  let component: ModalListarProductosComponent;
  let fixture: ComponentFixture<ModalListarProductosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalListarProductosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalListarProductosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
