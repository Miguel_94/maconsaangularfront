import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { BsModalRef } from 'ngx-bootstrap/modal';
import {ProductoService} from '../../service/producto.service';
import {IngresarComponent} from '../Compra/ingresar/ingresar.component';
import { ProductoModule } from 'src/app/models/producto/producto.module';
import { KardexComponent } from '../kardex/kardex.component';

@Component({
  selector: 'app-modal-listar-productos',
  templateUrl: './modal-listar-productos.component.html',
  styleUrls: ['./modal-listar-productos.component.css']
})
export class ModalListarProductosComponent implements OnInit {

  constructor(public modalRef: BsModalRef,public productoService:ProductoService) { }

  public title;
  public ingresarCompraComponent:IngresarComponent;
  public kardexComponent:KardexComponent;

  ngOnInit() {
    this.listarProductos();
  }

  public listarProductos()
  {
    this.productoService.listarProducto()
    .subscribe(res=>{
      this.productoService.productos=res as ProductoModule[];
      console.log(res);
    });
  }

 public seleccionarProductoDoubleClick(producto:ProductoModule){

    if(this.ingresarCompraComponent!=null)
    {
      this.ingresarCompraComponent.producto=producto;
      this.ingresarCompraComponent.selectedProduct=true;
    }
    else
    {
      this.kardexComponent.producto=producto;
    }
    this.modalRef.hide();
  }

  public seleccionarProducto(producto:ProductoModule){
   if(this.ingresarCompraComponent!=null)
    {
      this.ingresarCompraComponent.producto=producto;
      this.ingresarCompraComponent.selectedProduct=true;
    }
    else
    {
      this.kardexComponent.producto=producto;
    }
    this.modalRef.hide();
  }

}
