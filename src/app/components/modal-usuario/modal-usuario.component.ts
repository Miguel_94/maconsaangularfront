import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import {UsuarioComponent} from '../usuario/usuario.component';
import {UsuarioModule} from '../../models/usuario/usuario.module';
import {UsuarioService} from '../../service/usuario.service'; 
import { PersonaModule } from 'src/app/models/persona/persona.module';

@Component({
  selector: 'app-modal-usuario',
  templateUrl: './modal-usuario.component.html',
  styleUrls: ['./modal-usuario.component.css']
})
export class ModalUsuarioComponent implements OnInit {

  title;
  usuario:UsuarioModule;
  usuarioComponent:UsuarioComponent;
  public formUsuario: FormGroup;
  public formUsuarioPersona:FormGroup;
  editar:boolean=false;

  constructor(private formBuilder: FormBuilder,public modalRef: BsModalRef,public usuarioService:UsuarioService) { this.usuario=new UsuarioModule(); this.usuario.persona=new PersonaModule();}

  ngOnInit() {
    this.formUsuarioPersona=this.formBuilder.group({

      })
    this.formUsuario= this.formBuilder.group({

      idUsuario: [''],
      nombreUsuario: ['',Validators.required],
      password: ['',Validators.required],
      rutaDeAlmacenamientoReportes:[''],
      persona: this.formBuilder.group({
        dni: ['',Validators.required],
        nombres: ['',Validators.required],
        apellidos: ['',Validators.required],
        telefono: ['',Validators.required],
        imagen:['']
        }),
      estado:{
          idEstado:'1'
        }
     })
  }

  guardar(){
    console.log(this.formUsuario.value);
    if (this.formUsuario.valid) {

          this.usuarioService.ingresarUsuario(this.formUsuario.value).subscribe(res=>{
          this.formUsuario.reset();
          if(this.editar)
          {
            Swal.fire('Exito', 'Usuario Editado Correctamente!', 'success');  
          }else{
            Swal.fire('Exito', 'Usuario Registrado Correctamente!', 'success');
          }
          this.modalRef.hide();
          this.usuarioComponent.listarUsuarios();
        });  
      
    }else{
      Swal.fire('Error', 'Debes completar el formulario. Verifique por favor.', 'error'); 
    }
  }

}
