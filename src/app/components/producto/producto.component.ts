import { Component, OnInit,Inject,ViewChild,ElementRef,AfterViewInit  } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import {ModalComponent} from '../modal/modal.component'
import {ProductoService} from '../../service/producto.service';
import { DOCUMENT } from '@angular/common'; 
import {ProductoModule} from '../../models/producto/producto.module';
import Swal from 'sweetalert2';
import {PaginacionModule} from '../../models/paginacion/paginacion.module';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html'
})
export class ProductoComponent implements OnInit {

  modalRef: BsModalRef;
  producto:ProductoModule;
  busqueda='codigo';
  descripcionBusqueda: String ='';
  public paginado:any[]=[];
  public paginacion:PaginacionModule;
  tamaniosPagina:any[]=[];
  tamanioPaginado;

  constructor(private modalService: BsModalService,@Inject(DOCUMENT) document, public productoService : ProductoService) { 
  }
  @ViewChild('miInput',{static: false}) nombre: ElementRef;
  esValidoInput1="form-control";
  esValidoInput="valid-feedback";
  ngOnInit(){
    this.tamaniosPagina.push({ id:1,valor: 10 },{ id:2,valor: 30 },{ id:3,valor: 50 });
    this.listarProductosPaginado();
  }

  buscarProducto(){
    if(this.descripcionBusqueda!="")
    {
      var opcion=1;

      if(this.busqueda!="codigo")
      {
        opcion=2;
      }
      this.productoService.buscarProducto(this.descripcionBusqueda,opcion)
      .subscribe(res=>{
        this.productoService.productos=res as ProductoModule[];
        console.log(res);
      });
       
    }else{
      this.listarProductos();
    }
  }

  ver(){
    alert(this.busqueda);
  }

  public listarProductos()
  {
    this.productoService.listarProducto()
    .subscribe(res=>{
      this.productoService.productos=res as ProductoModule[];
      console.log(res);
    });
  }

  cambiarClase(){
    this.esValidoInput1="form-control is-valid";
    this.esValidoInput="valid-feedback";
  }
  cambiarClase2(){
    this.esValidoInput1="form-control is-invalid";
    this.esValidoInput="invalid-feedback";
  }
  /*
  ngAfterViewInit() {
   console.log(this.nombre); // aparece la propiedad nativeElement y dentro una gran cantidad de propiedades y métodos
    this.nombre.nativeElement.setAttribute('placeholder', 'Escriba su nombre');
    this.nombre.nativeElement.addClass('form-control');
    this.nombre.nativeElement.focus();
    }*/



    eliminarProducto(producto:ProductoModule)
    {
      Swal.fire({
        title: '¿Deseas eliminar este producto?',
        text: 'No podras recuperar este registro!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si, eliminar',
        cancelButtonText: 'Cancelar'
      }).then((result) => {
        if (result.value) {
            this.productoService.eliminarProducto(producto.idProducto).subscribe(res=>{
          Swal.fire(
            'Eliminado!',
            'El registro ha sido eliminado correctamente.',
            'success'
          )
          this.listarProductos();
        }) 
      }
    })
  }
  

    editarProducto(producto:ProductoModule){
      this.modalRef = this.modalService.show(ModalComponent,  {
        initialState: {
          editar:true,
          title: 'Editar Producto',
          producto:producto,
          productoComponent:this
          }
      });
    }

    editarProductoDoubleClick(producto:ProductoModule)
    {
      this.modalRef = this.modalService.show(ModalComponent,  {
        initialState: {
          editar:true,
          title: 'Editar Producto',
          producto:producto,
          productoComponent:this
          }
      });
    }


  ingresarProducto() {
    this.modalRef = this.modalService.show(ModalComponent,  {
      initialState: {
        title: 'Ingresar Producto',
        productoComponent:this
      }
    });
  }

  gestionarPaginado(pagina: any){
    console.log(pagina.valor);
    switch (pagina.valor) {
    case 'Siguiente':
      this.paginacion.numeroPagina = this.paginacion.numeroPagina + 1;
      break;

    case 'Anterior':
      this.paginacion.numeroPagina = this.paginacion.numeroPagina - 1;
      break;

    default:
      this.paginacion.numeroPagina = parseInt(pagina.valor, 0);
      break;
    }
  this.obtenerPaginado();
  }

  public listarProductosPaginado()
  {
    this.paginacion=new PaginacionModule();
    this.productoService.listarProductoPaginado(this.paginacion)
    .subscribe(res=>{
      this.paginacion=res as PaginacionModule;
      this.paginado=this.construirPaginado(res);
      this.productoService.productos=this.paginacion.listaFilas;
      console.log(this.paginacion);
    });
  }

  private obtenerPaginado() {
    this.paginacion.primeraFila=(this.paginacion.numeroPagina-1)*this.paginacion.tamanioPagina;
    this.productoService.listarProductoPaginado(this.paginacion)
    .subscribe(res=>{
      this.paginacion=res as PaginacionModule;
      this.paginado=this.construirPaginado(res);
      this.productoService.productos=this.paginacion.listaFilas;
      console.log(this.paginacion);
    });
  }


  private construirPaginado(objeto): any[] {
    let valor;
    let paginaInicial;
    let paginaFinal;
    const lstPagina: any[] = [];

    if (objeto.numeroPagina > 0) {
        valor = Math.floor(objeto.numeroPagina / 5);
        valor = (objeto.numeroPagina % 5 === 0) ? (valor - 1) : valor;
        paginaInicial = (5 * valor) + 1;
        paginaFinal = 5 * (valor + 1);

        if (paginaFinal >= objeto.totalPaginas) {
            paginaFinal = objeto.totalPaginas;
        }

        if (objeto.numeroPagina === 1) {
            lstPagina.push({ clase: 'page-item disabled', valor: 'Anterior' });
        } else {
            lstPagina.push({ clase: 'page-item', valor: 'Anterior' });
        }

        for (let i = paginaInicial; i <= paginaFinal; i++) {
            if (objeto.numeroPagina === i) {
                lstPagina.push({ clase: 'page-item active', valor: i });
            } else {
                lstPagina.push({ clase: 'page-item', valor: i });
            }
        }

        if (objeto.numeroPagina === objeto.totalPaginas) {
            lstPagina.push({ clase: 'page-item disabled', valor: 'Siguiente' });
        } else {
            lstPagina.push({ clase: 'page-item', valor: 'Siguiente' });
        }
    } else {
        lstPagina.push({ clase: 'page-item disabled', valor: 'Anterior' });
        lstPagina.push({ clase: 'page-item disabled', valor: 'Siguiente' });
    }

    return lstPagina;
  }

  onChangeMostrar()
  {
    switch (this.tamanioPaginado) {
      case 1:
          this.paginacion.tamanioPagina=10;
          break;
      case 2:
          this.paginacion.tamanioPagina=30;
          break;
      case 3:
          this.paginacion.tamanioPagina=50;
          break;
      default:
          this.paginacion.tamanioPagina=10;
          break;    
    }
    this.paginacion.numeroPagina=1;
    this.obtenerPaginado();
    
  }

}
