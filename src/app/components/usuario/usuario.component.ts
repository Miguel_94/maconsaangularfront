import { Component, OnInit } from '@angular/core';
import {UsuarioModule} from '../../models/usuario/usuario.module';
import {UsuarioService} from '../../service/usuario.service';
import {ModalUsuarioComponent} from '../modal-usuario/modal-usuario.component';
import Swal from 'sweetalert2';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent implements OnInit {

  usuario:UsuarioModule;
  busqueda='ID';
  descripcionBusqueda: String ='';
  modalRef: BsModalRef;

  constructor(public usuarioService:UsuarioService,private modalService: BsModalService) {
   }

  ngOnInit() {
    this.listarUsuarios();
  }

  buscarUsuario()
  {
    if(this.descripcionBusqueda!="")
    {
      var opcion=1;

      if(this.busqueda!="ID")
      {
        opcion=2;
      }
      this.usuarioService.buscarUsuario(this.descripcionBusqueda,opcion)
      .subscribe(res=>{
        this.usuarioService.usuarios=res as UsuarioModule[];
        console.log(res);
      });
       
    }else{
      this.listarUsuarios();
    }
  }
  ingresarUsuario(){
    this.modalRef = this.modalService.show(ModalUsuarioComponent,  {
      initialState: {
        title: 'Ingresar Usuario',
        usuarioComponent:this
      }, class: "modal-xl" 
    },);
  }

  listarUsuarios(){
    this.usuarioService.listarUsuario()
    .subscribe(res=>{
      this.usuarioService.usuarios=res as UsuarioModule[];
      console.log(res);
    });
  }

  eliminarUsuario(usuario:UsuarioModule)
  {
    Swal.fire({
      title: '¿Deseas eliminar este producto?',
      text: 'No podras recuperar este registro!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, eliminar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) {
          this.usuarioService.eliminarUsuario(usuario.idUsuario).subscribe(res=>{
        Swal.fire(
          'Eliminado!',
          'El registro ha sido eliminado correctamente.',
          'success'
        )
        this.listarUsuarios();
      }) 
    }
  })
}

editarUsuario(usuario:UsuarioModule){
  this.modalRef = this.modalService.show(ModalUsuarioComponent,  {
    initialState: {
      editar:true,
      title: 'Editar Usuario',
      usuario:usuario,
      usuarioComponent:this
      }, class: "modal-lg" 
  });
}

editarUsuarioDoubleClick(usuario:UsuarioModule)
{
  this.modalRef = this.modalService.show(ModalUsuarioComponent,  {
    initialState: {
      editar:true,
      title: 'Editar Usuario',
      usuario:usuario,
      usuarioComponent:this
      }, class: "modal-lg" 
  });
}


}
