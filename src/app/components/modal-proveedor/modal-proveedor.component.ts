import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import {ProveedorService} from '../../service/proveedor.service';
import {ProveedorModule} from '../../models/proveedor/proveedor.module';
import {ProveedorComponent} from '../proveedor/proveedor.component';
 
@Component({
  selector: 'app-modal-proveedor',
  templateUrl: './modal-proveedor.component.html',
  styleUrls: ['./modal-proveedor.component.css']
})
export class ModalProveedorComponent implements OnInit {

  constructor(private formBuilder: FormBuilder,public modalRef: BsModalRef,public proveedorService:ProveedorService) {
      this.proveedor=new ProveedorModule();
   }
  title;
  proveedor:ProveedorModule;
  public formProveedor: FormGroup;
  editar:boolean=false;
  proveedorComponent:ProveedorComponent;

  ngOnInit() {

    this.formProveedor= this.formBuilder.group({

      ruc: ['',Validators.required],
      denominacion: ['',Validators.required],
      direccion: ['',Validators.required],
      telefono: ['',Validators.required],
      estado:{
        idEstado:1
      }
    })
  }

  guardar(){
    console.log(this.formProveedor.value);
    if (this.formProveedor.valid) {

          this.proveedorService.ingresarProveedor(this.formProveedor.value).subscribe(res=>{
          this.formProveedor.reset();
          if(this.editar)
          {
            Swal.fire('Exito', 'Proveedor Editado Correctamente!', 'success');  
          }else{
            Swal.fire('Exito', 'Proveedor Registrado Correctamente!', 'success');
          }
         // this.productoComponent.listarProductos();
          this.modalRef.hide();
          this.proveedorComponent.listarProveedor();
        });  
      
    }else{
      Swal.fire('Error', 'Debes completar el formulario. Verifique por favor.', 'error'); 
    }
  }

}
