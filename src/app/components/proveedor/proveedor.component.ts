import { Component, OnInit } from '@angular/core';
import {ModalProveedorComponent} from '../modal-proveedor/modal-proveedor.component'
import Swal from 'sweetalert2';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import {ProveedorService} from '../../service/proveedor.service'; 
import { ProveedorModule } from 'src/app/models/proveedor/proveedor.module';
 
@Component({
  selector: 'app-proveedor',
  templateUrl: './proveedor.component.html',
  styleUrls: ['./proveedor.component.css']
})
export class ProveedorComponent implements OnInit {

  modalRef: BsModalRef;
  descripcionBusqueda:String="";
  busqueda="ruc";
  constructor(private modalService: BsModalService, private proveedorService:ProveedorService) { }

  ngOnInit() {
    this.listarProveedor();
  }

  public listarProveedor()
  {
    this.proveedorService.listarProveedor()
    .subscribe(res=>{
      this.proveedorService.proveedores=res as ProveedorModule[];
      console.log(res);
    });
  }

  buscarProveedor(){
    if(this.descripcionBusqueda!="")
    {
      var opcion=1;

      if(this.busqueda!="ruc")
      {
        opcion=2;
      }
      this.proveedorService.buscarProveedor(this.descripcionBusqueda,opcion)
      .subscribe(res=>{
        this.proveedorService.proveedores=res as ProveedorModule[];
        console.log(res);
      });
       
    }else{
      this.listarProveedor();
    }
  }

  ingresarProveedor() {
    this.modalRef = this.modalService.show(ModalProveedorComponent,  {
      initialState: {
        title: 'Ingresar Proveedor'
      }
    });
  }
  editarProveedorDoubleClick(proveedor:ProveedorModule){
    this.modalRef = this.modalService.show(ModalProveedorComponent,  {
      initialState: {
        editar:true,
        title: 'Editar Proveedor',
        proveedor:proveedor,
        proveedorComponent:this
        }
    });
  }

  editarProveedor(proveedor:ProveedorModule){
    this.modalRef = this.modalService.show(ModalProveedorComponent,  {
      initialState: {
        editar:true,
        title: 'Editar Proveedor',
        proveedor:proveedor,
        proveedorComponent:this
        }
    });
  }

  eliminarProveedor(proveedor:ProveedorModule){
    Swal.fire({
      title: '¿Deseas eliminar este proveedor?',
      text: 'No podras recuperar este registro!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, eliminar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) {
          this.proveedorService.eliminarProveedor(proveedor.ruc).subscribe(res=>{
        Swal.fire(
          'Eliminado!',
          'El registro ha sido eliminado correctamente.',
          'success'
        )
        this.listarProveedor();
      }) 
    }
  })
  }

}
