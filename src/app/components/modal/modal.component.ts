import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {ProductoModule} from '../../models/producto/producto.module';
import Swal from 'sweetalert2';
import {ProductoService} from '../../service/producto.service';
import {ProductoComponent} from '../producto/producto.component'

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {
  title;
  producto:ProductoModule;
  editar:boolean=false;
  productoComponent;
  esValidoInput1="form-control";
  esValidoInput="valid-feedback";
  
  constructor(private formBuilder: FormBuilder,public modalRef: BsModalRef, public productoService:ProductoService) { 
    this.producto=new ProductoModule();
  }
  public formProducto: FormGroup;

  ngOnInit() {

    this.formProducto= this.formBuilder.group({
      idProducto: [''],
      descripcion: ['',Validators.required],
      cantidad: ['',Validators.required],
      valorVenta: ['',Validators.required],
      unidadDeMedida: ['',Validators.required],
      estado:{
        idEstado:1
      }
    })
  }

  guardar(){

    //console.log(this.formProducto.value);
    if (this.formProducto.valid) {

          this.productoService.ingresarProducto(this.formProducto.value).subscribe(res=>{
          this.formProducto.reset();
          if(this.editar)
          {
            Swal.fire('Exito', 'Producto Editado Correctamente!', 'success');  
          }else{
            Swal.fire('Exito', 'Producto Registrado Correctamente!', 'success');
          }
          this.productoComponent.listarProductos();
          this.modalRef.hide();
        });  
      
    }else{
      Swal.fire('Error', 'Debes completar el formulario. Verifique por favor.', 'error'); 
    }
  }

  InputDescripcionValida(){
    //alert('aca');
    this.esValidoInput1="form-control is-valid";
    //this.esValidoInput="valid-feedback";
  }

  InputDescripcionInvalida(){
    //alert('aca');
    this.esValidoInput1="form-control is-invalid";
    //this.esValidoInput="valid-feedback";
  }




  /*
  Swal.fire({
  title: 'Are you sure?',
  text: 'You will not be able to recover this imaginary file!',
  type: 'warning',
  showCancelButton: true,
  confirmButtonText: 'Yes, delete it!',
  cancelButtonText: 'No, keep it'
}).then((result) => {
  if (result.value) {
    Swal.fire(
      'Deleted!',
      'Your imaginary file has been deleted.',
      'success'
    )
  // For more information about handling dismissals please visit
  // https://sweetalert2.github.io/#handling-dismissals
  } else if (result.dismiss === Swal.DismissReason.cancel) {
    Swal.fire(
      'Cancelled',
      'Your imaginary file is safe :)',
      'error'
    )
  }
})
  */
}
