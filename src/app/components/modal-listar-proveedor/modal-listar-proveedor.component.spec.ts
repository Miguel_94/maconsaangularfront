import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalListarProveedorComponent } from './modal-listar-proveedor.component';

describe('ModalListarProveedorComponent', () => {
  let component: ModalListarProveedorComponent;
  let fixture: ComponentFixture<ModalListarProveedorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalListarProveedorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalListarProveedorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
