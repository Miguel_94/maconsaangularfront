import { Component, OnInit } from '@angular/core';
import {IngresarComponent} from '../Compra/ingresar/ingresar.component';
import {ProveedorModule} from '../../models/proveedor/proveedor.module';
import Swal from 'sweetalert2';
import { BsModalRef } from 'ngx-bootstrap/modal';
import {ProveedorService} from '../../service/proveedor.service';
import { ProductoModule } from 'src/app/models/producto/producto.module';

@Component({
  selector: 'app-modal-listar-proveedor',
  templateUrl: './modal-listar-proveedor.component.html',
  styleUrls: ['./modal-listar-proveedor.component.css']
})
export class ModalListarProveedorComponent implements OnInit {

  constructor(public modalRef: BsModalRef, public proveedorService:ProveedorService) { }

  public title;
  public ingresarCompraComponent:IngresarComponent;

  ngOnInit() {
    this.listarProveedor();
  }

  listarProveedor()
  {
    this.proveedorService.listarProveedor()
    .subscribe(res=>{
      this.proveedorService.proveedores=res as ProveedorModule[];
      console.log(res);
    }); 
  }

  seleccionarProveedor(proveedor:ProveedorModule)
  {

    this.ingresarCompraComponent.compra.proveedor=proveedor;
    this.modalRef.hide();
  }

  seleccionarProveedorDoubleClick(proveedor:ProveedorModule){

    this.ingresarCompraComponent.compra.proveedor=proveedor;
    this.modalRef.hide();
  }

}
