import { Component, OnInit } from '@angular/core';
import { CompraServiceService} from '../../../service/compra-service.service';
import {CompraModule} from '../../../models/compra/compra.module';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import {ModalDetalleCompraComponent} from '../../modal-detalle-compra/modal-detalle-compra.component';

@Component({
  selector: 'app-listar',
  templateUrl: './listar.component.html',
  styleUrls: ['./listar.component.css'] 
})
export class ListarComponent implements OnInit {

  constructor(public compraService:CompraServiceService,private modalService: BsModalService) { }

  compra:CompraModule;
  modalRef:BsModalRef;

  ngOnInit() {
    this.listarCompra();
  }

public listarCompra()
  {
    this.compraService.listarCompra()
    .subscribe(res=>{
      
      this.compraService.compras=res as CompraModule[];
      this.compraService.compras.forEach(element => {
        element.fecha=new Date(element.fecha);
        console.log(new Date(element.fecha));  
      });
    });
  }


  detalleCompra(compra:CompraModule) {
    console.log(compra);
    this.modalRef = this.modalService.show(ModalDetalleCompraComponent,  {
      initialState: {
        title: 'Detalle Compra'
      },class:"modal-xl" 
    });
  }

  detalleCompraDoubleClick(compra:CompraModule){
    this.modalRef = this.modalService.show(ModalDetalleCompraComponent,  {
      initialState: {
        title: 'Detalle Compra'
      },class:"modal-xl" 
    });
  }


}
