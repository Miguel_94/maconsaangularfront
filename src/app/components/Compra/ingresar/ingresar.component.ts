import { Component, OnInit,ViewChild,ElementRef  } from '@angular/core';
import {CompraModule} from '../../../models/compra/compra.module';
import {ModalListarProductosComponent} from '../../modal-listar-productos/modal-listar-productos.component';
import {ModalListarProveedorComponent} from '../../modal-listar-proveedor/modal-listar-proveedor.component';
import {ProductoModule} from '../../../models/producto/producto.module';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import Swal from 'sweetalert2';
import {CompraServiceService} from '../../../service/compra-service.service';
import {Router} from '@angular/router';
import { DetalleComprobanteDePagoModule } from 'src/app/models/detalle-comprobante-de-pago/detalle-comprobante-de-pago.module';

@Component({
  selector: 'app-ingresar',
  templateUrl: './ingresar.component.html',
  styleUrls: ['./ingresar.component.css']
})
export class IngresarComponent implements OnInit {


  constructor(private modalService: BsModalService,public compraService:CompraServiceService,private router:Router) {
   this.compra=new CompraModule();
   
  }

  public compra:CompraModule;
  public modalRef:BsModalRef;
  public producto:ProductoModule;
  public selectedProduct:boolean=false;
  public cantidad:number=0;
  public precioCompra:number=0;
  public detalleComprobanteDePago:DetalleComprobanteDePagoModule;
  public totalSinIgv:number=0;

  @ViewChild("btnAgregarProductos",{static: false}) btnAgregarProductos: ElementRef;
  @ViewChild("txtDescripcionProducto",{static: false}) txtDescripcionProducto: ElementRef;
  @ViewChild("txtUnidadDeMedida",{static: false}) txtUnidadDeMedida: ElementRef;
  

  ngOnInit() {

    this.producto=new ProductoModule();
    this.compra=new CompraModule();
    this.listarCarrito();
    
  }

  disable(){
    this.btnAgregarProductos.nativeElement.disabled=false;
  }
  enable(){
    this.btnAgregarProductos.nativeElement.disabled=true;
   } 

   listarCarrito()
   {
    this.compraService.listarCarrito().subscribe(res=>{
      this.compra.comprobanteDePago.detalleComprobanteDePago=res as DetalleComprobanteDePagoModule[]; 
      this.calcularTotalSinIGV(this.compra.comprobanteDePago.detalleComprobanteDePago);
      console.log(res);
    });
   }

  listaDeProveedores(){

    this.modalRef = this.modalService.show(ModalListarProveedorComponent,  {
      initialState: {
        title: 'Proveedores',
        ingresarCompraComponent:this
        }, class: "modal-lg" 
    });
  }

  listaDeProductos(){

    this.modalRef = this.modalService.show(ModalListarProductosComponent,  {
      initialState: {
        title: 'Productos',
        ingresarCompraComponent:this
        }, class: "modal-xl" 
    });
  }

  agregarProductoCarrito()
  {
    //console.log(this.producto);
    this.detalleComprobanteDePago=new DetalleComprobanteDePagoModule();
    this.detalleComprobanteDePago.producto=this.producto;
    this.detalleComprobanteDePago.cantidadCompra=this.cantidad;
    this.detalleComprobanteDePago.valorVenta=this.precioCompra;
    this.detalleComprobanteDePago.subtotal=this.cantidad*this.precioCompra;
    this.cantidad=0;
    this.precioCompra=0;
    this.selectedProduct=false;
   // this.txtDescripcionProducto.nativeElement.value="";
    //this.txtUnidadDeMedida.nativeElement.value="";
    this.compraService.agregarDetalleComprobante(this.detalleComprobanteDePago).subscribe(res=>{
     // console.log(res);
      this.listarCarrito();
    });
    this.producto=new ProductoModule();
    
  }

  calcularTotalSinIGV(detallesComprobanteDePago:DetalleComprobanteDePagoModule[])
  {
    this.totalSinIgv=0;
    detallesComprobanteDePago.forEach(element => {
      this.totalSinIgv+=element.subtotal;
    });
  }

  editarDetalleComprobante(detalleComprobanteDePago:DetalleComprobanteDePagoModule){
    //console.log(detalleComprobanteDePago.producto);
    this.producto=detalleComprobanteDePago.producto;
    this.precioCompra=detalleComprobanteDePago.valorVenta;
    this.cantidad=detalleComprobanteDePago.cantidadCompra;
    this.selectedProduct=true;
    //Swal.fire('Exito', 'Editado Correctamente!', 'success'); 
  }

  eliminarDetalleComprobante(detalleComprobanteDePago:DetalleComprobanteDePagoModule)
  {
    console.log(detalleComprobanteDePago);
    Swal.fire({
      title: '¿Deseas remover este producto?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, eliminar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) {
          this.compraService.eliminarDetalleComprobante(detalleComprobanteDePago).subscribe(res=>{
        Swal.fire(
          'Eliminado!',
          'El registro ha sido removido correctamente.',
          'success'
        )
        //console.log(res);
        this.listarCarrito();
        this.producto=new ProductoModule();
        this.precioCompra=0;
        this.cantidad=0;
      }) 
    }
  })
  }

  guardarCompra(){
    console.log(this.compra);
    if(this.compra.comprobanteDePago.numero!=null && this.compra.comprobanteDePago.serie!=null 
     && this.compra.comprobanteDePago.detalleComprobanteDePago.length>0 && this.compra.proveedor.ruc!=null)
    {
      this.compraService.guardarCompra(this.compra).subscribe(res=>{

        console.log(res);
        Swal.fire({
          type: 'success',
          title: 'Compra Guardada Correctamente',
          showConfirmButton: true,
          timer: 1500
        })
        setTimeout(() => {
          this.router.navigate(['/compra'])
        }
        , 1500);

      });
      //Swal.fire('Exito', 'Compra Guardada Correctamente!', 'success'); 
        
    }
    else{
      Swal.fire('Error', 'Debes completar el registro para continuar. Verifique por favor. ', 'error'); 
    }
  }
}
