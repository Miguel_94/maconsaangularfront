import { Component, OnInit } from '@angular/core';
import {InventarioServiceService} from '../../service/inventario-service.service';
import { ProductoModule } from 'src/app/models/producto/producto.module';

@Component({
  selector: 'app-inventario',
  templateUrl: './inventario.component.html',
  styleUrls: ['./inventario.component.css']
})
export class InventarioComponent implements OnInit {

  public costoInventario:number;
  public totalProductosInventario:number;
  
  constructor(public inventarioService:InventarioServiceService) { }

  ngOnInit() {
    this.inventario();
  }

  inventario()
  {
    this.inventarioService.productosInventario()
    .subscribe(res=>{
      this.inventarioService.listaProductosInventario=res as ProductoModule[];
      console.log(res);
    });

    this.inventarioService.costoInventario()
    .subscribe(res=>{
      this.costoInventario=res as number;
      console.log(res);
    });

    this.inventarioService.totalProductos()
    .subscribe(res=>{
      this.totalProductosInventario=res as number;
      console.log(res);
    });
  }

}
