import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DetalleComprobanteDePagoModule } from '../detalle-comprobante-de-pago/detalle-comprobante-de-pago.module';
import { EstadoModule } from '../estado/estado.module';
import { TipoDeComprobanteModule } from '../tipo-de-comprobante/tipo-de-comprobante.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class ComprobanteDePagoModule {
  
  public idComprobanteDePago:number;
  public numero:String;
  public serie:String;
  public estado:EstadoModule;
  public detalleComprobanteDePago:DetalleComprobanteDePagoModule[]=[];
  public tipodecomprobantedepago:TipoDeComprobanteModule;

  constructor(){

    this.tipodecomprobantedepago=new TipoDeComprobanteModule();
    this.tipodecomprobantedepago.idTipoDeComprobanteDePago=1;

  }

}
