import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {EstadoModule} from '../estado/estado.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class ProveedorModule {

  public ruc:String;
  public descripcion:String;
  public direccion:String;
  public telefono:String;
  public estado:EstadoModule;

 }
