import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductoModule } from '../producto/producto.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class DetalleComprobanteDePagoModule { 
  public idDetalleComprobanteDePago:number;
  public producto:ProductoModule;
  public subtotal:number;
  public cantidadCompra:number;
  public valorVenta:number;
  public cantidadFinal:number;
}
