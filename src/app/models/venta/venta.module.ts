import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComprobanteDePagoModule } from '../comprobante-de-pago/comprobante-de-pago.module';
import { UsuarioModule } from '../usuario/usuario.module';
import { EstadoModule } from '../estado/estado.module';




@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class VentaModule {

  public idVenta:number;
  public comprobanteDePago:ComprobanteDePagoModule;
  public cliente:String;
  public rucCliente:String;
  public usuario:UsuarioModule;
  public fecha:Date;
  public estado:EstadoModule;

 }
