import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProveedorModule } from '../proveedor/proveedor.module';
import { EstadoModule } from '../estado/estado.module';
import { ComprobanteDePagoModule } from '../comprobante-de-pago/comprobante-de-pago.module';
import { UsuarioModule } from '../usuario/usuario.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class CompraModule {

  
  public idCompra:number;
  public proveedor:ProveedorModule;
  public estado:EstadoModule;
  public comprobanteDePago:ComprobanteDePagoModule;
  public fecha:Date;
  public usuario:UsuarioModule;

  constructor(){
    this.comprobanteDePago=new ComprobanteDePagoModule();
    this.usuario=new UsuarioModule();
    this.proveedor=new ProveedorModule();
  }
  
 }
