import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductoModule } from '../producto/producto.module';
import { CompraModule } from '../compra/compra.module';
import { VentaModule } from '../venta/venta.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class KardexModule {

  public idKardex:number;
  public producto:ProductoModule;
  public listaCompras:CompraModule[];
  public listaVentas:VentaModule[];
  public stockInicial:number;
  public fechaInicio:Date;
  

 }
