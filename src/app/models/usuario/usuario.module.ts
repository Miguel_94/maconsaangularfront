import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PersonaModule} from '../persona/persona.module'
import {EstadoModule} from '../estado/estado.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class UsuarioModule { 

    public idUsuario:number;
    public persona:PersonaModule;
    public nombreUsuario:String;
    public password:String;
    public rutaDeAlmacenamientoReportes:String;
    public estado:EstadoModule;

}
