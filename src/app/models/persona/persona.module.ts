import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class PersonaModule { 

  public dni:String;
  public nombres:String;
  public apellidos:String;
  public telefono:String;
  public imagen:String;

}
