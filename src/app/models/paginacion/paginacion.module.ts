import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThrowStmt } from '@angular/compiler';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class PaginacionModule {
  
  public primeraFila:number;
  public totalPaginas:number;
  public tamanioPagina:number;
  public ultimaPagina:number;
  public numeroPagina:number;
  public ultimaFila:number;
  public totalRegistros:number;
  public listaFilas:any[]=[];

  constructor()
  {
    this.primeraFila=0;
    this.totalPaginas=0;
    this.tamanioPagina=10;
    this.ultimaPagina=0;
    this.numeroPagina=1;
    this.ultimaFila=0;
    this.totalRegistros=0;
  }

}
