import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {EstadoModule} from '../estado/estado.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class ProductoModule { 

  public idProducto:number;
  public descripcion:String;
  public unidadDeMedida: String;
  public cantidad: number;
  public valorVenta : number;
  public estado:EstadoModule;

}
