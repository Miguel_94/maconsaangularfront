import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { ModalModule } from 'ngx-bootstrap';


import { AppComponent } from './app.component';
import { ProductoComponent } from './components/producto/producto.component';
import { ProveedorComponent } from './components/proveedor/proveedor.component';
import { ModalComponent } from './components/modal/modal.component';
import { UsuarioComponent } from './components/usuario/usuario.component';


import{ProductoService} from './service/producto.service';
import{ProveedorService} from './service/proveedor.service';
import {UsuarioService} from './service/usuario.service';
import {CompraServiceService} from './service/compra-service.service';
import {VentaServiceService} from './service/venta-service.service';

import { ModalProveedorComponent } from './components/modal-proveedor/modal-proveedor.component';
import { ModalUsuarioComponent } from './components/modal-usuario/modal-usuario.component';
import { ListarComponent } from './components/Compra/listar/listar.component';
import { IngresarComponent } from './components/Compra/ingresar/ingresar.component';
import { InventarioComponent } from './components/inventario/inventario.component';
import { KardexComponent } from './components/kardex/kardex.component';
import { ModalDetalleCompraComponent } from './components/modal-detalle-compra/modal-detalle-compra.component';
import { ModalListarProductosComponent } from './components/modal-listar-productos/modal-listar-productos.component';
import { ModalListarProveedorComponent } from './components/modal-listar-proveedor/modal-listar-proveedor.component';

@NgModule({
  declarations: [
    AppComponent,
    ProductoComponent,
    ProveedorComponent,
    ModalComponent,
    UsuarioComponent,
    ModalProveedorComponent,
    ModalUsuarioComponent,
    ListarComponent,
    IngresarComponent,
    InventarioComponent,
    KardexComponent,
    ModalDetalleCompraComponent,
    ModalListarProductosComponent,
    ModalListarProveedorComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    ModalModule.forRoot()
  ],
  providers: [ProductoService,ProveedorService,UsuarioService,CompraServiceService,VentaServiceService],
  bootstrap: [AppComponent],
  entryComponents: [ModalComponent,ModalProveedorComponent,ModalUsuarioComponent,ModalDetalleCompraComponent,ModalListarProductosComponent,
    ModalListarProveedorComponent ]
})
export class AppModule { }
