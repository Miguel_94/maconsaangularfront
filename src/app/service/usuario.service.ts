import { Injectable } from '@angular/core';
import {UsuarioModule} from '../models/usuario/usuario.module';
import { HttpClient, HttpHeaders} from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor(public httpClient:HttpClient) { }
  readonly URL_API='http://localhost:8080/Usuario/';
   usuarios:UsuarioModule[];

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json; charset=ISO-8859-15', 'Accept': 'application/json'})
  };


  public ingresarUsuario(usuario:UsuarioModule){
    return this.httpClient.post(this.URL_API+"Guardar",usuario);
  }


  public listarUsuario(){
    return this.httpClient.get(this.URL_API+"Listar");
  }

  public buscarUsuario(descripcion:String,opcion:number){
    return this.httpClient.get(this.URL_API+"Buscar"+ `${descripcion}`+"?opcion="+`${opcion}`);
  }
  
  eliminarUsuario(ID:number){
    return this.httpClient.get(this.URL_API +"Eliminar"+ `${ID}`);
  }
}
