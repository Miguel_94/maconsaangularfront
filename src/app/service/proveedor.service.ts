import { Injectable } from '@angular/core';
import {ProveedorModule} from '../models/proveedor/proveedor.module';
import { HttpClient, HttpHeaders} from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class ProveedorService {

  readonly URL_API='http://localhost:8080/Proveedor/';

  constructor(public httpClient:HttpClient) { }

  proveedores:ProveedorModule[];
  
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json; charset=ISO-8859-15', 'Accept': 'application/json'})
  };

  public ingresarProveedor(proveedor:ProveedorModule){
    return this.httpClient.post(this.URL_API+"Guardar",proveedor);
  }


  public listarProveedor(){
    return this.httpClient.get(this.URL_API+"Listar");
  }

  public buscarProveedor(descripcion:String,opcion:number){
    return this.httpClient.get(this.URL_API+"Buscar"+ `${descripcion}`+"?opcion="+`${opcion}`);
  }
  
  eliminarProveedor(ruc:String){
    return this.httpClient.get(this.URL_API +"Eliminar"+ `${ruc}`);
  }

}
