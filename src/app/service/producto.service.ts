import { Injectable } from '@angular/core';
import {ProductoComponent} from '../components/producto/producto.component'
import { HttpClient, HttpHeaders} from '@angular/common/http'
import { ProductoModule } from '../models/producto/producto.module';
import {PaginacionModule} from '../models/paginacion/paginacion.module';

@Injectable({
  providedIn: 'root'
})
export class ProductoService {

  readonly URL_API='http://localhost:8080/Producto/';

  constructor(private httpClient:HttpClient) { }

  private producto:ProductoComponent;
  productos:ProductoModule[];
  
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json; charset=ISO-8859-15', 'Accept': 'application/json'})
  };

  public ingresarProducto(producto:ProductoComponent){
    return this.httpClient.post(this.URL_API+"Guardar",producto);
  }


  public listarProducto(){
    return this.httpClient.get(this.URL_API+"Listar");
  }

  public buscarProducto(descripcion:String,opcion:number){
    return this.httpClient.get(this.URL_API+"Buscar"+ `${descripcion}`+"?opcion="+`${opcion}`);
  }
/*
  public eliminarProducto(producto:ProductoModule)
  {
    return this.httpClient.post(this.URL_API+"Eliminar",producto);
  }
*/

  listarProductoPaginado(paginacion:PaginacionModule){
    //return this.httpClient.get(this.URL_API+"ListarPaginado?posicion=10&tamanioPagina=10&numeroPagina=1");
    return this.httpClient.post(this.URL_API+"ListarPaginado",paginacion);
  }

  eliminarProducto(idProducto:number){
    return this.httpClient.get(this.URL_API +"Eliminar"+ `${idProducto}`);
  }


}
