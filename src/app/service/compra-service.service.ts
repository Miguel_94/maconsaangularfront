import { Injectable } from '@angular/core';
import {CompraModule} from '../models/compra/compra.module';
import {ListarComponent} from '../components/Compra/listar/listar.component'
import { HttpClient, HttpHeaders} from '@angular/common/http'
import { DetalleComprobanteDePagoModule } from '../models/detalle-comprobante-de-pago/detalle-comprobante-de-pago.module';

@Injectable({
  providedIn: 'root'
})
export class CompraServiceService {

  constructor(private httpClient:HttpClient) { 
  }
  readonly URL_API='http://localhost:8080/Compra/';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json; charset=ISO-8859-15', 'Accept': 'application/json'})
  };

  compras:CompraModule[];

  public listarCompra(){
    return this.httpClient.get(this.URL_API+"Listar");
  }

  public ingresarCompra(compra:CompraModule){
    return this.httpClient.post(this.URL_API+"Guardar",compra);

  }

  public agregarDetalleComprobante(detalleComprobanteDePago:DetalleComprobanteDePagoModule){
    return this.httpClient.post(this.URL_API+"AgregarDetalle",detalleComprobanteDePago);

  }

  public eliminarDetalleComprobante(detalleComprobanteDePago:DetalleComprobanteDePagoModule)
  {
    return this.httpClient.post(this.URL_API+"EliminarDetalle",detalleComprobanteDePago);
  }

  public guardarCompra(compra:CompraModule)
  {
    return this.httpClient.post(this.URL_API+"Guardar",compra);
  }
  public listarCarrito()
  {
    return this.httpClient.get(this.URL_API+"listarCarrito");
  }

}
