import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http'
import {ProductoModule} from '../models/producto/producto.module';
import {CompraModule} from '../models/compra/compra.module';
import {VentaModule} from '../models/venta/venta.module';

@Injectable({
  providedIn: 'root'
})
export class InventarioServiceService {

  readonly URL_API='http://localhost:8080/';

  listaProductosInventario:ProductoModule[];

  constructor(private httpClient:HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json; charset=ISO-8859-15', 'Accept': 'application/json'})
  };

   productosInventario()
    {
      return this.httpClient.get(this.URL_API+"productosInventario");
    }

    totalProductos()
    {
      return this.httpClient.get(this.URL_API+"totalProductos");
    }

    costoInventario()
    {
      return this.httpClient.get(this.URL_API+"CostoInventario");
    }

    listaVentasPeriodo(anio:number,mes:number,idProducto:number){
      return this.httpClient.get(this.URL_API+"listarVentasPeriodo?idProducto="+ `${idProducto}`+
      "&mes="+`${mes}`+"&anio="+`${anio}`);
    }

    listaComprasPeriodo(anio:number,mes:number,idProducto:number){
      return this.httpClient.get(this.URL_API+"listarComprasPeriodo?idProducto="+ `${idProducto}`+
      "&mes="+`${mes}`+"&anio="+`${anio}`);
    }

}
