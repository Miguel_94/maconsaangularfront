import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { ProductoComponent } from './components/producto/producto.component';
import { ProveedorComponent } from './components/proveedor/proveedor.component';
import { UsuarioComponent } from './components/usuario/usuario.component';
import {ListarComponent} from './components/Compra/listar/listar.component';
import {IngresarComponent} from './components/Compra/ingresar/ingresar.component';
import {InventarioComponent} from './components/inventario/inventario.component';
import {KardexComponent} from './components/kardex/kardex.component';

const routes: Routes = [

  { path: 'producto', component: ProductoComponent },
  { path: 'proveedor', component: ProveedorComponent },
  { path: 'usuario', component: UsuarioComponent },
  { path: 'compra',component:ListarComponent},
  { path: 'compra/listar', component:IngresarComponent},
  { path: 'inventario', component:InventarioComponent},
  { path: 'kardex', component:KardexComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
